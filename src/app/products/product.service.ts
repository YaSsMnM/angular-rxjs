import { ProductCategoryService } from './../product-categories/product-category.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError, combineLatest, Subject, merge, from } from 'rxjs';
import { catchError, map, scan, tap, shareReplay, mergeMap, toArray, filter, switchMap } from 'rxjs/operators';

import { Product } from './product';
import { Supplier } from '../suppliers/supplier';
import { SupplierService } from '../suppliers/supplier.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private productsUrl = 'api/productss';
  // To test error handling use incorrect path for exp : private productsUrl = 'api/productss';

  private suppliersUrl = this.supplierService.suppliersUrl;

  // Combining Stream to retrive category name
  productsC$ = this.http.get<Product[]>(this.productsUrl).pipe(
    catchError(this.handleError)
  );


  productsWithCategory$ = combineLatest([
    this.productsC$,
    this.productCategoryService.productsCategories$]).pipe(
      map(([products, categories]) =>
        products.map(product => ({
          ...product,
          price: product.price * 1.5,
          searchKey: [product.productName],
          category: categories.find(c => product.categoryId === c.id).name
        }) as Product)
      ),
      tap(data => console.log('Products: ', JSON.stringify(data))),
      shareReplay(1),
    );


  // Make the code more declarative use products$ instead of use a methode getProducts()
  products$ = this.http.get<Product[]>(this.productsUrl).pipe(
    map(products =>
      products.map(product => ({
        ...product,
        price: product.price * 1.5,
        searchKey: [product.productName]
      }) as Product)
    ),
    tap(data => console.log('Products: ', JSON.stringify(data))),
    catchError(this.handleError)
  );

  private selectedProductSubject = new Subject<number>();
  selectedProductAction$ = this.selectedProductSubject.asObservable();

  selectedProduct$ = combineLatest([
    this.productsWithCategory$,
    this.selectedProductAction$]).pipe(
      map(([products, selectedProduct]) => (products.find(product => product.id === selectedProduct))),
      tap(product => console.log('selected product: ', product)),
      shareReplay(1)
    );

  private productInsertedSubject = new Subject<Product>();
  productInsertedAction$ = this.productInsertedSubject.asObservable();

  // The merge function emit elements from both streams into one output stream
  // merge and scan work well together when reacting to an add operation
  productsWithAdd$ = merge(
    this.productsWithCategory$,
    this.productInsertedAction$).pipe(
      scan((acc: Product[], value: Product) => [...acc, value])
    );


  // Get it all (all suppliers)
  selectProductSuppliers$ = combineLatest([
    this.selectedProduct$,
    this.supplierService.suppliers$]).pipe(
      map(([selectedProduct, suppliers]) =>
        suppliers.filter(supplier =>
          selectedProduct.supplierIds.includes(supplier.id))),
      tap(data => console.log('Products: ', JSON.stringify(data))),
      shareReplay(1),
      catchError(this.handleError)
    );

  // Just in time (get only concerned supplier)
  // Use this technique anytime you need to use ids from one stream to retrive related data just when it needed
  selectProductSuppliersWithMerge$ = this.selectedProduct$.pipe(
    filter(selectedProduct => Boolean(selectedProduct)),
    switchMap(selectedProduct => from(selectedProduct.supplierIds).pipe(
      mergeMap(supplierId => this.http.get<Supplier>(`${this.suppliersUrl}/ ${supplierId}`)),
      toArray(),
      tap(suppliers => console.log('product suppliers: ', JSON.stringify(suppliers))),
    )));

  constructor(private http: HttpClient,
              private supplierService: SupplierService, private productCategoryService: ProductCategoryService) { }


  addProduct(newProduct?: Product): void {
    newProduct = newProduct || this.fakeProduct();
    this.productInsertedSubject.next(newProduct);
  }

  setSelectedProductSubject(productId: number): void {
    this.selectedProductSubject.next(productId);
  }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.productsUrl)
      .pipe(
        map(products =>
          products.map(product => ({
            ...product,
            price: product.price * 1.5,
            searchKey: [product.productName]
          }) as Product)
        ),
        tap(data => console.log('Products: ', JSON.stringify(data))),
        catchError(this.handleError)
      );
  }

  private fakeProduct(): Product {
    return {
      id: 42,
      productName: 'Another One',
      productCode: 'TBX-0042',
      description: 'Our new product',
      price: 8.9,
      categoryId: 3,
      category: 'Toolbox',
      quantityInStock: 30
    };
  }

  private handleError(err: any): Observable<never> {
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
    }
    console.error(err);
    return throwError(errorMessage);
  }

}
