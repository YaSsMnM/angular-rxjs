import { Component, OnInit, OnDestroy } from '@angular/core';

import { of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'pm-product-list',
  templateUrl: './product-list-alt.component.html'
})
export class ProductListAltComponent /*implements OnInit, OnDestroy*/ {
  pageTitle = 'Products';
  errorMessage = '';

  // products: Product[] = [];
  // sub: Subscription;

  // Make code more declarative , so no need to ngOnInit()
  products$ = this.productService.productsWithCategory$.pipe(
    catchError(error => {
      this.errorMessage = error;
      return of([]);  // or EMPTY;
    })
  );

  selectedProductId$ = this.productService.selectedProduct$;

  constructor(private productService: ProductService) { }

  // ngOnInit(): void {
  //   this.sub = this.productService.getProducts().subscribe(
  //     products => this.products = products,
  //     error => this.errorMessage = error
  //   );
  // }

  // No need ,automatically unsubscribe
  // ngOnDestroy(): void {
  //   this.sub.unsubscribe();
  // }

  onSelected(productId: number): void {
    this.productService.setSelectedProductSubject(productId);
  }
}
