import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { throwError, Observable, of } from 'rxjs';
import { Supplier } from './supplier';
import { catchError, concatMap, map, mergeMap, shareReplay, switchMap, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SupplierService {
  suppliersUrl = 'api/suppliers';

  constructor(private http: HttpClient) {
    this.supplierWithconcatMap$.subscribe(res => console.log('concatMap:', res));
    this.supplierWithmergeMap$.subscribe(res => console.log('mergeMap:', res));
    this.supplierWithswitchMap$.subscribe(res => console.log('switchMap:', res));
  }

  suppliers$ = this.http.get<Supplier[]>(this.suppliersUrl).pipe(
    tap(data => console.log('suppliers: ', JSON.stringify(data))),
    shareReplay(1),
    catchError(this.handleError)
  );

  supplierWithconcatMap$ = of(1, 5, 8).pipe(
    tap(id => console.log('concatMap source:', id)),
    concatMap(id => this.http.get<Supplier>(`${this.suppliersUrl}/${id}`))
  );

  supplierWithmergeMap$ = of(1, 5, 8).pipe(
    tap(id => console.log('mergeMap source:', id)),
    mergeMap(id => this.http.get<Supplier>(`${this.suppliersUrl}/${id}`))
  );

  supplierWithswitchMap$ = of(1, 5, 8).pipe(
    tap(id => console.log('switchMap source:', id)),
    switchMap(id => this.http.get<Supplier>(`${this.suppliersUrl}/${id}`))
  );


  private handleError(err: any): Observable<never> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Backend returned code ${err.status}: ${err.body.error}`;
    }
    console.error(err);
    return throwError(errorMessage);
  }

}
