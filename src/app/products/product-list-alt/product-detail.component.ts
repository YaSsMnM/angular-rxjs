import { Component } from '@angular/core';
import { EMPTY, combineLatest } from 'rxjs';
import { catchError, filter, map } from 'rxjs/operators';
import { Product } from '../product';

import { ProductService } from '../product.service';

@Component({
  selector: 'pm-product-detail',
  templateUrl: './product-detail.component.html'
})
export class ProductDetailComponent {
  errorMessage = '';

  product$ = this.productService.selectedProduct$.pipe(
    catchError(error => {
      this.errorMessage = error;
      return EMPTY;
    })
  );

  pageTitle$ = this.product$.pipe(
    map((p: Product) => p ? `Product details dor : ${p.productName}` : null)
  );

  productSuppliers$ = this.productService.selectProductSuppliersWithMerge$.pipe(
    catchError(error => {
      this.errorMessage = error;
      return EMPTY;
    })
  );

  // Combine all streams for product detail in one stream vm to use in template
  vm$ = combineLatest([
    this.product$,
    this.productSuppliers$,
    this.pageTitle$
  ]).pipe(
    filter(([product]) => Boolean(product)),
    map(([product, productSuppliers, pageTitle]) =>
      ({ product, productSuppliers, pageTitle })
    )
  );

  constructor(private productService: ProductService) { }
}
