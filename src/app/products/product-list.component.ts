import { catchError, filter, map, startWith } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription, Observable, of, EMPTY, Subject, combineLatest, BehaviorSubject } from 'rxjs';

import { Product } from './product';
import { ProductService } from './product.service';
import { ProductCategoryService } from '../product-categories/product-category.service';

@Component({
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent /*implements OnInit, OnDestroy*/ {
  pageTitle = 'Product List';
  errorMessage = '';

  // products: Product[] = [];
  // products$: Observable<Product[]>; // we replace the product array with Observable of product array

  // sub: Subscription;

  // private categorySelectedSubject = new Subject<number>(); Subject don't have initial value
  // so we get a blank list for the first time when we go to Product List

  private categorySelectedSubject = new BehaviorSubject<number>(0);
  categorySelectedAction$ = this.categorySelectedSubject.asObservable();

  categories$ = this.productCategoryService.productsCategories$.pipe(
    catchError(error => {
      this.errorMessage = error;
      return EMPTY;
    })
  );

  productsSimpleFilter$ = combineLatest([this.productService.productsWithAdd$, this.categorySelectedAction$
    // To avoid this problem with Subject we can use startWith operator that provides an initial value
    // this.categorySelectedAction$.pipe(
    //   startWith(0)
    // )
  ]).pipe(
    map(([products, selectedCategoryId]) =>
      products.filter(product =>
        selectedCategoryId ? product.categoryId === selectedCategoryId : true
      )), catchError(error => {
        this.errorMessage = error;
        return EMPTY;
      })
  );


  // Combine all streams for product detail in one stream vm to use in template
  vm$ = combineLatest([this.categories$, this.productsSimpleFilter$]).pipe(
    map(([categories, products]) => ({ categories, products }))
  );

  // Get products with categories names
  // productsWithCategory$ = this.productService.productsWithCategory$.pipe(
  //   catchError(error => {
  //     this.errorMessage = error;
  //     return of([]);  // or EMPTY;
  //   })
  // );

  // Make code more declarative , so no need to ngOnInit()
  // products$ = this.productService.products$.pipe(
  //   catchError(error => {
  //     this.errorMessage = error;
  //     return of([]);  // or EMPTY;
  //   })
  // );


  constructor(private productService: ProductService,
              private productCategoryService: ProductCategoryService) { }

  // ngOnInit(): void {
  //   // this.sub = this.productService.getProducts()
  //   //   .subscribe(
  //   //     products => this.products = products,
  //   //     error => this.errorMessage = error
  //   //   );
  //   this.products$ = this.productService.getProducts().pipe(
  //     catchError(error => {
  //       this.errorMessage = error;
  //       return of([]);  // or EMPTY;
  //     });
  //   );
  // }


  // No need ,automatically unsubscribe
  // ngOnDestroy(): void {
  //   this.sub.unsubscribe();
  // }

  onAdd(): void {
    this.productService.addProduct();
  }

  onSelected(categoryId: string): void {
    this.categorySelectedSubject.next(+categoryId);
  }
}
